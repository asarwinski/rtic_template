#![no_main]
#![no_std]

use panic_semihosting as _;
use stm32h7xx_hal as _;
use stm32h7xx_hal::prelude::*;
use stm32h7xx_hal::gpio::{Output, PushPull, PB14};
use cortex_m_semihosting::hprintln;
use systick_monotonic::Systick;

#[rtic::app(device = stm32h7xx_hal::pac, peripherals = true, dispatchers = [UART8])]
mod app {
    use super::*;
    
    #[monotonic(binds = SysTick, default = true)]
    type MyMono = Systick<100>; // 100 Hz / 10 ms granularity

    #[shared]
    struct Shared {
        led_on: bool,
    }

    #[local]
    struct Local {
        led: PB14<Output<PushPull>>,
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        hprintln!("init");

        let pwr = cx.device.PWR.constrain();
        let pwrcfg = pwr.freeze();

        let rcc = cx.device.RCC.constrain();

        let mono = Systick::new(cx.core.SYST, 100_000_000);

        let ccdr = rcc.sys_ck(100.MHz()).freeze(pwrcfg, &cx.device.SYSCFG);

        let gpioa = cx.device.GPIOB.split(ccdr.peripheral.GPIOB);
        let mut led = gpioa.pb14.into_push_pull_output();
        led.set_low();

        task_blink::spawn_after(1.secs().into()).unwrap();

        (
            Shared {
                led_on: false
            },
            Local {
                led
            },
            init::Monotonics(mono)
        )
    }

    #[idle(shared = [led_on])]
    fn idle(mut cx: idle::Context) -> ! {
        hprintln!("idle");

        let mut now = monotonics::now();
        loop {
            if (monotonics::now() - now).cmp(&1.secs().into()).is_gt() {
                hprintln!("Switching LED");
                cx.shared.led_on.lock(|led_on| {
                    *led_on = !*led_on;
                });
                now = monotonics::now();
                task_blink::spawn().unwrap();
            }
        }
    }

    #[task(shared = [led_on], local = [led])]
    fn task_blink(mut cx: task_blink::Context) {
        defmt::info!("Hello from task1!");
        match cx.shared.led_on.lock(|led_on| {*led_on}) {
            true  => cx.local.led.set_high(),
            false => cx.local.led.set_low(),
        }
    }
}
